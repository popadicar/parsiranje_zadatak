#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "all_caps.c"
#include "most_common_letter.c"
#include "palindrom_check.c"
#include "no_letters.c"

const char* all_caps(char *s); //zbog length-a
char most_common_letter(char *s);
const char* no_letters(char *s);
int palindrom_check(char *s);