#include "function.h"
#include <stdio.h>

int main(int argc, char **argv)
{

	if (argc < 3)
	{
		printf("Mora postojati bar 3 stringa!!!\n");
		exit (EXIT_FAILURE);
	}

	int opcija = atoi(argv[argc - 1]);

	switch(opcija)
	{
		case 1: 

			for (int i = 1; i<argc-1; i++)
				printf("%s\n", all_caps(argv[i]));
		  break;
		
		case 2:
			for (int i = 1; i<argc-1; i++)
				printf("%c\n", most_common_letter(argv[i]));
		  break;
		case 3: 
			for (int i =1; i<argc-1; i++)
				printf("%s\n", no_letters(argv[i]));
		  break;
		
		case 4:
			for(int i = 1; i<argc-1; i++) 
			{
				if( palindrom_check(argv[i]) != 0)
					printf("%s: string koji ste napisali JESTE palindrom!\n", argv[i]);
				else
					printf("%s: string koji ste napisali NIJE palindrom!\n", argv[i]);
			}
		  break;
		  
		default: 
			printf("Nepostojeca opcija!!!\n");
			printf("Pozvati kao: ./parsiranje prvi_string drugi_string ... opcija[1,4]\n");
		  break;
	}
	exit(EXIT_SUCCESS);
}
