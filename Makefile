all:
	gcc  -o parsiranje main.c
	
all_caps.o: all_caps.c
	gcc -c all_caps.c
	
most_common_letter.o: most_common_letter.c
	gcc -c most_common_letter.c
	
no_letters.o: no_letters.c
	gcc -c no_letters.c
	
palindrom_check.o: palindrom_check.c
	gcc -c palindrom_check.c
	
main.o: function.h main.c
	gcc -c main.c
	
clean:
	rm main.o main
	rm -f parsiranje *.o
