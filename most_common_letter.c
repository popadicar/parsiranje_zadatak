
//NAJCESCE POJAVLJIVANJE SLOVA

char most_common_letter(char *str)
{
  	char result;
  	int i, len;
  	int max = -1;

  	int frequency[1024] = {0};

  	len = strlen(str);

  	for(i = 0; i < len; i++)
  	{
  		frequency[str[i]]++;
	}

  	for(i = 0; i < len; i++)
  	{
		if(max < frequency[str[i]])
		{
			max = frequency[str[i]];
			result = str[i];
		}
	}
	return result;
}

